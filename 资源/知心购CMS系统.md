# 知心购CMS系统

## 1. 项目初始化

### 1.1项目技术栈

> `vue-cli4.0`:https://cli.vuejs.org/zh/
>
> `vuex`:https://vuex.vuejs.org/zh/
>
> `vue-router`:https://router.vuejs.org/zh/
>
> `axios`:http://www.axios-js.com/
>
> `element-ui`:https://element.eleme.cn/#/zh-CN/component/installation
>
> `scss`:https://www.sass.hk/
>
> `animate.css`:https://daneden.github.io/animate.css/
>
> `ES6`:`promise/awiat、async`

### 1.2 `vue-cli` 初始化项目

## 2. 公共样式和组件库导入

### 2.1 安装`element-ui`

1. 安装`element-ui`

```js
npm i element-ui -S
```
2. `main.js`中集成`element-ui`

```js
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);
```

### 2.2 以下样式代码拷贝到`style`文件夹中，并在`main.js`中导入

> **`variable.scss`**文件

```scss
$dark-blue: #3a3f51;
$bg-gary: #f0f3f4;
$text-color: #606266;
$white: #ffffff;

$sideBarWidth: 200px;
$px30: 30px;
$px20: 20px;

%width-height {
  width: 100%;
  height: 100%;
}
```

> **`transition`**文件

```scss
// transition scss
.fade-page-enter-active,
.fade-page-leave-active {
  transition: all 0.5s;
}
.fade-page-enter {
  opacity: 0;
  transform: translateX(-30px);
}
.fade-page-leave-to {
  opacity: 0;
  transform: translateX(-30px);
}
```

> **`layout.scss`**文件

```scss
#app {
  .wrapper {
    @extend %width-height;
    position: relative;
    background-color: $bg-gary;
    .wrapper_con {
      height: calc(100% - 50px);
      width: 100%;
      // margin-top: 50px;
      padding-top: 50px;
    }
  }
  .pageMain {
    margin-left: 200px;
    transition: all 0.3s;
    height: 100%;
    & > div {
      padding: 36px 30px;
      background: $bg-gary;
    }
  }
  .noPadding {
    padding: 0 !important;
  }
}
// sideBar
#app {
  .sideBar {
    width: $sideBarWidth;
    height: calc(100% - 50px);
    background-color: $dark-blue;
    position: fixed;
    bottom: 0;
    left: 0;
    overflow: hidden;
    transition: width 0.3s;
    z-index: 999;
    .el-scrollbar {
      height: 100%;
      .el-scrollbar__wrap {
        overflow-x: hidden !important;
      }
    }
    .el-menu {
      @extend %width-height;
      border: none;
    }
  }
}
// header
.header {
  height: 50px;
  position: fixed;
  top: 0;
  width: 100%;
  left: 0;
  z-index: 999;
}
.header_l {
  width: 200px;
  height: 100%;
  float: left;
  background-color: #3a3f51;
  box-sizing: border-box;
  border-bottom: 1px solid #454b61;
  transition: width 0.3s;
  overflow: hidden;
}
.header_r {
  height: 100%;
  margin-left: 200px;
  background-color: $white;
  box-shadow: 0 2px 2px rgba(0, 0, 0, 0.05), 0 1px 0 rgba(0, 0, 0, 0.05);
  transition: all 0.3s;
}
.logoLink {
  color: $white !important;
  font-size: 15px;
  margin-left: 18px;
  display: inline-block;
  font-weight: bold;
  height: 100%;
  line-height: 50px;
  img {
    height: 28px;
    vertical-align: middle;
    margin-right: 8px;
    position: relative;
    top: -3px;
  }
}
.headr_d1 {
  float: left;
  height: 100%;
  margin-left: 20px;
  .sidecoll {
    float: left;
    margin-top: 16px;
  }
  .bread {
    float: left;
    margin: 18px 0 0 20px;
  }
}
.headr_d2 {
  float: right;
  height: 100%;
  .userDrop {
    height: 50px;
    .userDrop_text {
      margin-right: 8px;
      i {
        margin-left: 2px;
      }
    }
    img {
      width: 40px;
      height: 40px;
      border-radius: 50%;
      vertical-align: middle;
    }
  }
  .userDd {
    .el-dropdown-menu__item {
      padding: 0 12px;
    }
  }
  .headrUl {
    height: 100%;
    li {
      float: left;
      height: 100%;
      line-height: 50px;
      padding: 0 10px;
      cursor: pointer;
      .iconFont {
        font-size: 18px;
        height: 50px;
        vertical-align: initial;
        line-height: 50px;
      }
      .el-badge__content.is-fixed.is-dot {
        right: 9px;
        top: 16px;
      }
      &:hover {
        background-color: #f6f8f8;
      }
      &:last-child {
        padding: 0 20px 0 10px;
      }
    }
  }
}
.el-dropdown-menu__item {
  i {
    font-size: 16px;
  }
  a {
    display: block;
    width: 100%;
    height: 100%;
  }
}
// collapse
#app {
  .closeBar {
    .sideBar {
      width: 64px !important;
      .el-menu--collapse > .sideItem .el-menu-item [class^='el-icon-'],
      .el-menu--collapse
        > .sideItem
        .el-submenu
        > .el-submenu__title
        [class^='el-icon-'] {
        margin: 0;
        vertical-align: middle;
        width: 24px;
        text-align: center;
      }
      .el-menu--collapse > .sideItem .el-menu-item span,
      .el-menu--collapse > .sideItem .el-submenu > .el-submenu__title span {
        height: 0;
        width: 0;
        overflow: hidden;
        visibility: hidden;
        display: inline-block;
      }
      .el-menu--collapse > .sideItem .el-menu-item .el-submenu__icon-arrow,
      .el-menu--collapse
        > .sideItem
        .el-submenu
        > .el-submenu__title
        .el-submenu__icon-arrow {
        display: none;
      }
    }
    .header_l {
      width: 64px !important;
      overflow: hidden;
    }
    .header_r,
    .pageMain {
      margin-left: 64px;
    }
  }
}
```

> **`index.scss`**文件

```scss
@import './variable.scss';
@import './layout.scss';
@import './transition.scss';

* {
  margin: 0;
  padding: 0;
}
html {
  height: 100%;
}
body {
  height: 100%;
  font: 12px/1.5 Helvetica Neue, Helvetica, PingFang SC, Hiragino Sans GB,
    Microsoft YaHei, SimSun, sans-serif;
  margin: 0;
  padding: 0;
}
table {
  border-collapse: collapse;
  border-spacing: 0;
}
ol,ul {
  list-style: none;
}
h1,h2,h3,h4,h5,h6 {
  font-size: 100%;
  font-weight: normal;
}
a {
  color: inherit;
  text-decoration: none;
  color: #606266;
  &:hover {
    text-decoration: none;
    color: #409eff;
  }
}
#app {
  height: 100%;
}
.mtop30 {
  margin-top: $px30;
}
.fontBold {
  font-weight: bold;
}
.mtrt20 {
  margin-right: $px20;
}
.wid300 {
  width: 300px !important;
}
.btnStyle1 {
  padding: 11px 20px !important;
}
.header i[class^='el-icon'] {
  color: $text-color;
}
#app .clearFixed:after {
  content: '';
  display: block;
  width: 0;
  height: 0;
  clear: both;
  visibility: hidden;
}
.loginCon .el-input__inner {
  background: #fff;
  height: 50px;
  outline: none;
  color: $text-color;
  border: none;
}
.loginCon .el-form-item {
  border: 1px solid #ddd;
  border-radius: 5px;
}
.marginLeft {
  margin-left: 64px !important;
}
.pTitle {
  font-size: 14px;
  color: #666;
  padding: 10px 0;
  code {
    background-color: #f9fafc;
    padding: 0 4px;
    border: 1px solid #eaeefb;
    border-radius: 4px;
  }
  span {
    font-size: 12px;
  }
  i {
    margin-right: 5px;
    color: #ffc107;
    font-size: 18px;
  }
}
.el-notification {
  .el-icon-s-opportunity {
    color: #ffc107;
    font-size: 22px;
    margin-top: 2px;
  }
}
```

## 3. 新建`api`文件夹，封装请求文件

### 3.1 `api`文件夹下新建`index.js`，书写以下代码

```js
import axios from 'axios'
import Qs from 'qs'
import store from '@/store'
import router from '@/router'
import Vue from 'vue'
import { Loading, Message } from 'element-ui' // 引用element-ui的加载和消息提示组件

const $axios = axios.create({
  // 设置超时时间
  timeout: 30000,
  // 基础url，会在请求url中自动添加前置链接
  baseURL: 'http://127.0.0.1:8888/api/private/v1/'
})
Vue.prototype.$http = axios // 并发请求
// 在全局请求和响应拦截器中添加请求状态
let loading = null

// 请求拦截器
$axios.interceptors.request.use(
  config => {
    loading = Loading.service({ text: '拼命加载中' })
    const token = store.getters.token
    if (token) {
      config.headers.Authorization = token // 请求头部添加token
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)
// 响应拦截器
$axios.interceptors.response.use(
  response => {
    if (loading) {
      loading.close()
    }
    const code = response.status
    if ((code >= 200 && code < 300) || code === 304) {
      return Promise.resolve(response.data)
    } else {
      return Promise.reject(response)
    }
  },
  error => {
    if (loading) {
      loading.close()
    }
    if (error.response) {
      switch (error.response.status) {
        case 401:
          // 返回401 清除token信息并跳转到登陆页面
          store.commit('DEL_TOKEN')
          router.replace('/login')
          break
        case 404:
          Message.error('网络请求不存在')
          break
        default:
          Message.error(error.response.data.message)
      }
    } else {
      // 请求超时或者网络有问题
      if (error.message.includes('timeout')) {
        Message.error('请求超时！请检查网络是否正常')
      } else {
        Message.error('请求失败，请检查网络是否已连接')
      }
    }
    return Promise.reject(error)
  }
)

// get，post请求方法
export default {
  post(url, data) {
    return $axios({
      method: 'post',
      url,
      data: Qs.stringify(data),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    })
  },
  get(url, params) {
    return $axios({
      method: 'get',
      url,
      params
    })
  }
}
```

### 3.2 `api`文件夹下新建`login.js`，封装登录相关的请求

```js
import $axios from './index'

export function login(data) {
  const url = '/login'
  return $axios.post(url, data)
}
```

## 4. 登录功能

### 4.0 创建登录组件实现路由跳转

```js
{
    name: 'login',
    path: '/login',
    component: () => import('@/views/login/login') // 路由懒加载
}
```

### 4.1 登录界面

```html
<div class="login">
    <div class="loginBox">
        <h2 class="loginH2"><strong>知心购</strong> 后台管理系统</h2>
        <div class="loginCon">
            <div class="titleDiv">
                <h3>知行合壹</h3>
                <p>以简御繁 知行合壹</p>
                <i class="el-icon-collection-tag"></i>
            </div>
            <el-form ref="loginForm" :rules="rules" :model="ruleForm">
                <el-form-item prop="user">
                    <el-input placeholder="请输入账号" prefix-icon="el-icon-user" v-model="ruleForm.username"></el-input>
                </el-form-item>
                <el-form-item prop="password">
                    <el-input placeholder="请输入密码" prefix-icon="el-icon-lock" v-model="ruleForm.password" show-password></el-input>
                </el-form-item>
                <el-button type="primary" class="loginBtn" @click="loginHandler('loginForm')">登录</el-button>
            </el-form>
        </div>
    </div>
</div>
```

### 4.2 登录样式

```scss
.login {
    height: 100%;
    width: 100%;
    background: url(../../assets/pageBg/loginBg.jpg) no-repeat center center;
    background-size: 100% 100%;
    overflow: hidden;

    .loginBox {
        height: 455px;
        width: 550px;
        margin: 0 auto;
        position: relative;
        top: 50%;
        margin-top: -287px;

        .loginH2 {
            font-size: 38px;
            color: #fff;
            text-align: center;
        }

        .loginCon {
            margin-top: 30px;
            background: #eee;
            border-radius: 4px;

            .titleDiv {
                padding: 0 28px;
                background: #fff;
                position: relative;
                height: 120px;
                border-radius: 4px 4px 0 0;

                h3 {
                    font-size: 22px;
                    color: #555;
                    font-weight: initial;
                    padding-top: 23px;
                }

                p {
                    font-size: 16px;
                    color: #888;
                    padding-top: 12px;
                }

                i {
                    font-size: 48px;
                    color: #ddd;
                    position: absolute;
                    right: 27px;
                    top: 29px;
                }
            }

            .el-form {
                padding: 25px 25px 30px 25px;
                background: #eee;
                border-radius: 0 0 4px 4px;

                .loginBtn {
                    width: 100%;
                    background: #19b9e7;
                }
            }
        }
    }
}
```

### 4.3 登录逻辑

```js
data() {
    return {
        ruleForm: {
            username: 'admin',
            password: '123456'
        },
        rules: {
            username: [{
                    required: true,
                    message: '请输入用户名',
                    trigger: 'blur'
                },
                {
                    min: 3,
                    max: 15,
                    message: '长度在3到5个字符',
                    trigger: 'blur'
                }
            ],
            password: [{
                required: true,
                message: '请输入密码',
                trigger: 'blur'
            }]
        }
    }
},
methods: {
    loginHandler(form) {
        this.$refs[form].validate(valid => {
            if (valid) {
                this.fetchLogin()
            } else {
                return
            }
        })
    },
    fetchLogin() {
        const params = this.ruleForm
        login(params).then(res => {
            if (res.meta.status == 200) {
                this.$router.replace('/')
            }
        })
    }
}
```

### 4.4 用户登录凭证存储

1. `store`中定义登录请求以及需要存储的信息

```js
const state = {
  token: localStorage.getItem('token') ? localStorage.getItem('token') : '', // 认证凭证'
  userName: ''
}

const mutations = {
  SET_TOKEN(state, val) {
    state.token = val
    localStorage.setItem('token', val)
  },
  DEL_TOKEN(state) {
    state.token = ''
    state.userName = ''
    localStorage.removeItem('token')
  }
}

const actions = {
  fetch_login({ commit }, formdatas) {
    return new Promise((resolve, reject) => {
      login(formdatas)
        .then(res => {
          console.log(res)
          if (res.meta.status === 200) {
            Message.success(res.meta.msg)
            commit('SET_TOKEN', res.data.token)
          } else {
            Message.error(res.data.msg)
          }
          resolve(res)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
}
```

2. 登录界面调用`store`中的请求方式

```js
fetchLogin() {
    const params = this.ruleForm
    this.$store
        .dispatch('fetch_login', params)
        .then(res => {
            if (res.meta.status == 200) {
                this.$router.replace('/')
            }else{
                location.reload()
            }
        })
        .catch(error => {
            this.$message.error(error)
        })
}
```

## 5. 行为验证

### 5.1 登陆行为验证

> 导入 行为验证组件 SlideVerify， 放置在 components文件夹中

### 5.2 登陆行为验证界面

```html
<div class="slideShadow" v-show="showSlide">
      <transition>
        <div class="slideSty animated bounce">
          <slide-verify @success="onSuccess" @fail="onFail" :w="350" :h="175" ref="slideDiv"></slide-verify>
          <div class="iconBtn">
            <i class="el-icon-circle-close" @click="showSlide = false"></i>
            <i class="el-icon-refresh" @click="refresh"></i>
          </div>
        </div>
      </transition>
 </div>
```

### 5.3 登陆行为验证样式

```scss
.slideShadow {
    position: fixed;
    z-index: 999;
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.6);

    .slideSty {
      position: absolute;
      width: 380px;
      height: 311px;
      background: #e8e8e8;
      border: 1px solid #dcdcdc;
      left: 50%;
      top: 50%;
      margin-left: -188px;
      margin-top: -176px;
      z-index: 99;
      border-radius: 5px;

      .slide-verify {
        margin: 13px auto 0 auto;
        width: 350px !important;
      }

      .slide-verify-slider {
        width: 100% !important;
      }

      .slide-verify-refresh-icon {
        display: none;
      }

      .iconBtn {
        padding: 9px 0 0 19px;
        color: #5f5f5f;
        border-top: 1px solid #d8d8d8;
        margin-top: 17px;

        i {
          font-size: 22px;
          cursor: pointer;
        }

        i:last-child {
          margin-left: 7px;
        }
      }
    }
  }
```

### 5.4 登陆行为验证逻辑

```js
export default {
  data() {
    return {
      showSlide: false,
    }
  }
  methods: {
    // 滑动验证成功
    onSuccess() {
        this.showSlide = false
        this.fetchLogin()
    },
    // 滑动验证失败
    onFail() {
        Message.error('验证失败')
    },
    // 刷新滑动验证组件
    refresh() {
        this.$refs.slideDiv.refresh()
    }
  },
  components: {
    SlideVerify
  }
}
```



## 6. 首页布局

### 6.1 整体布局

> 1. 新建 `layout/index.vue` 组件
>
> 2. 新建 `layout/components` 文件夹
>
> 3. 将布局组件进行封装
>
>    <img src='images/01.png'>
>
> 4. 路由规则

```js
{
    name: 'Home',
    path: '/',
    component: () => import('@/layout'),
    redirect: '/dashbord',
    children: [
      {
        name: 'Dashbord',
        path: '/dashbord',
        component: () => import('@/views/dashbord') // 路由懒加载
      }
    ]
  }
```

```vue
<template>
<div class="wrapper" :class="{ closeBar: !opened }">
    <z-header></z-header>
    <div class="wrapper_con">
        <side-bar></side-bar>
        <page-main></page-main>
    </div>
</div>
</template>

<script>
import zHeader from './components/header'
import pageMain from './components/pageMain'
import sideBar from './components/sideBar'

import { mapState } from 'vuex'

export default {
    components: {
        zHeader,
        pageMain,
        sideBar
    },
    computed:{
        ...mapState(['opened'])
    }
}
</script>

<style scoped>

</style>

```





### 6.2 `header`组件

> 1. 组件内部代码

```vue
<template>
<div class="header">
    <div class="header_l">
        <router-link to="/" class="logoLink"><img src="@/assets/logo.png" alt="logo" />知心购</router-link>
    </div>
    <div class="header_r">
        <div class="headr_d1">
            <side-collapse class="sidecoll"></side-collapse>
        </div>
        <div class="headr_d2">
            <ul class="headrUl clearFixed">
                <li>
                    <full-screen></full-screen>
                </li>
                <li>
                    <user-dropdown></user-dropdown>
                </li>
            </ul>
        </div>
    </div>
</div>
</template>

<script>

import SideCollapse from '@/components/SideCollapse'
import UserDropdown from '@/components/UserDropdown'
import FullScreen from '@/components/FullScreen'
export default {
    components: {
        SideCollapse,
        UserDropdown,
        FullScreen
    }
}
</script>
```

#### 6.2.1 `FullScreen` 全屏组件

```vue
<template>
  <div>
    <i class="el-icon-full-screen iconFont" @click="toggleFull"></i>
  </div>
</template>

<script>
import screenfull from 'screenfull'
export default {
  methods: {
    toggleFull() {
      if (!screenfull.isEnabled) {
        this.$message({
          type: 'warning',
          message: 'you browser can not work'
        })
        return false
      }
      screenfull.toggle()
    }
  }
}
</script>
```

#### 6.2.2 `SideCollapse` 组件

```vue
<template>
<div class="sideCollapse" id="domColapse">
    <i :class="{ 'el-icon-s-unfold': !opened, 'el-icon-s-fold': opened }" @click="toggleOpen()"></i>
</div>
</template>

<script>
import { mapState,mapMutations } from 'vuex'
export default {
    methods: {
        ...mapMutations(['SET_OPENED']),
        toggleOpen() {
           this.SET_OPENED()
        }
    },
    computed:{
        ...mapState(['opened'])
    }
}
</script>

<style lang="scss" scoped>
.sideCollapse i {
    font-size: 18px;
    color: #363f44;
    cursor: pointer;
}
</style>
```

#### 6.2.3 `UserDropdown` 组件

```vue
<template>
<div>
    <el-dropdown class="userDd">
        <div class="userDrop">
            <span class="userDrop_text">admin<i class="el-icon-caret-bottom"></i></span>
            <img src="@/assets/avatar.jpg" alt="user" />
        </div>
        <el-dropdown-menu solt="dropdown">
            <el-dropdown-item>
                <router-link to="/"><i class="el-icon-s-home"></i>首页</router-link>
            </el-dropdown-item>
            <el-dropdown-item>
                <router-link to="/"><i class="el-icon-s-custom"></i>我的主页</router-link>
            </el-dropdown-item>
            <el-dropdown-item divided>
                <a href="javascript:void(0)" @click="loginOut"><i class="el-icon-switch-button"></i>登出</a>
            </el-dropdown-item>
        </el-dropdown-menu>
    </el-dropdown>
</div>
</template>

<script>
import {
    mapMutations
} from 'vuex'
export default {
    methods: {
        ...mapMutations(['DEL_TOKEN', 'DEL_USERNAME']),

        loginOut() {
            this.DEL_TOKEN()
            this.DEL_USERNAME()

            this.$router.options.routes[1].children.splice(1)
            this.$router.$addRoutes(this.$router.options.routes)
            
            this.$router.replace('/login')
        }
    }
}
</script>

```



### 6.3 `sideBar` 组件

> 1. 组件内部代码

```vue
<template>
<div class="sideBar">
    <el-scrollbar>
        <el-menu :unique-opened="true" :router="true" default-active="0" class="el-menu-vertical-demo" background-color="#3a3f51" text-color="#b5b6bd" active-text-color="rgb(79, 148, 212)" mode="vertical" :collapse-transition="false" :collapse="!opened">
            <el-menu-item index="/">
                <i class="el-icon-s-home"></i>
                <span slot="title">首页</span>
            </el-menu-item>
            <el-submenu index="1">
                <template slot="title">
                    <i class="el-icon-s-custom"></i>
                    <span>用户管理</span>
                </template>
                <el-menu-item index="/users">
                    <template slot="title">
                        <i class="el-icon-user"></i>
                        <span>用户列表</span>
                    </template>
                </el-menu-item>
            </el-submenu>
            <el-submenu index="2">
                <template slot="title">
                    <i class="el-icon-lock"></i>
                    <span>权限管理</span>
                </template>
                <el-menu-item index="2-1">
                    <template slot="title">
                        <i class="el-icon-unlock"></i>
                        <span>权限列表</span>
                    </template>
                </el-menu-item>
                <el-menu-item index="2-2"><template slot="title">
                        <i class="el-icon-watch"></i>
                        <span>角色列表</span>
                    </template></el-menu-item>
            </el-submenu>
            <el-submenu index="3">
                <template slot="title">
                    <i class="el-icon-s-goods"></i>
                    <span>商品管理</span>
                </template>
                <el-menu-item index="3-1">
                    <template slot="title">
                        <i class="el-icon-present"></i>
                        <span>商品列表</span>
                    </template>
                </el-menu-item>
                <el-menu-item index="3-2"><template slot="title">
                        <i class="el-icon-c-scale-to-original"></i>
                        <span>商品分类</span>
                    </template></el-menu-item>
            </el-submenu>
            <el-submenu index="4">
                <template slot="title">
                    <i class="el-icon-s-order"></i>
                    <span>订单管理</span>
                </template>
                <el-menu-item index="4-1">
                    <template slot="title">
                        <i class="el-icon-shopping-cart-1"></i>
                        <span>订单列表</span>
                    </template>
                </el-menu-item>
            </el-submenu>
            <el-submenu index="5">
                <template slot="title">
                    <i class="el-icon-s-tools"></i>
                    <span>系统设置</span>
                </template>
                <el-menu-item index="5-1">
                    <template slot="title">
                        <i class="el-icon-document"></i>
                        <span>访问日志</span>
                    </template>
                </el-menu-item>
                <el-menu-item index="5-2">
                    <template slot="title">
                        <i class="el-icon-edit-outline"></i>
                        <span>意见反馈</span>
                    </template>
                </el-menu-item>
            </el-submenu>
        </el-menu>
    </el-scrollbar>
</div>
</template>
```

### 6.4 `pageMain` 组件

```vue
<template>
  <div class="pageMain">
    <transition name="fade-page" mode="out-in">
      <router-view></router-view>
    </transition>
  </div>
</template>

<script>
export default {}
</script>
```

### 6.5 `dashboard` 组件

```vue
<template>
<div>
    <el-row :gutter="40">
        <el-col :span="8">
            <el-card shadow="hover" class="mgb40" style="height:252px;">
                <div class="user-info">
                    <img src="@/assets/avatar.jpg" class="user-avator" alt="">
                    <div class="user-info-cont">
                        <div class="user-info-name">admin</div>
                        <div>超级管理员</div>
                    </div>
                </div>
                <div class="user-info-list">上次登录时间：<span>2019-11-01</span></div>
                <div class="user-info-list">上次登录地点：<span>武汉</span></div>
            </el-card>
            <el-card shadow="hover" style="height:252px;font-size:16px;">
                <div slot="header" class="clearfix">
                    <span style="font-size:14px">语言详情</span>
                </div>
                Vue
                <el-progress :percentage="71.3" color="#42b983"></el-progress>
                JavaScript
                <el-progress :percentage="24.1" color="#f1e05a"></el-progress>
                CSS
                <el-progress :percentage="3.7"></el-progress>
                HTML
                <el-progress :percentage="0.9" color="#f56c6c"></el-progress>
            </el-card>
        </el-col>
        <el-col :span="16">
            <el-row :gutter="40" class="mgb40">
                <el-col :span="8">
                    <el-card shadow="hover" :body-style="{padding: '0px'}">
                        <div class="grid-content grid-con-1">
                            <i class="el-icon-user grid-con-icon"></i>
                            <div class="grid-cont-right">
                                <div class="grid-num">1234</div>
                                <div>用户访问量</div>
                            </div>
                        </div>
                    </el-card>
                </el-col>
                <el-col :span="8">
                    <el-card shadow="hover" :body-style="{padding: '0px'}">
                        <div class="grid-content grid-con-2">
                            <i class="el-icon-message grid-con-icon"></i>
                            <div class="grid-cont-right">
                                <div class="grid-num">321</div>
                                <div>系统消息</div>
                            </div>
                        </div>
                    </el-card>
                </el-col>
                <el-col :span="8">
                    <el-card shadow="hover" :body-style="{padding: '0px'}">
                        <div class="grid-content grid-con-3">
                            <i class="el-icon-shopping-cart-full grid-con-icon"></i>
                            <div class="grid-cont-right">
                                <div class="grid-num">5000</div>
                                <div>数量</div>
                            </div>
                        </div>
                    </el-card>
                </el-col>
            </el-row>
            <el-card shadow="hover" style="height:403px;">
                <div slot="header" class="clearfix" style="font-size:16px">
                    <span>待办事项</span>
                    <el-button style="float: right; padding: 3px 0" type="text">添加</el-button>
                </div>
                <el-table :data="todoList" :show-header="false" height="304" style="width: 100%;font-size:14px;">
                    <el-table-column width="40">
                        <template slot-scope="scope">
                            <el-checkbox v-model="scope.row.status"></el-checkbox>
                        </template>
                    </el-table-column>
                    <el-table-column>
                        <template slot-scope="scope">
                            <div class="todo-item" :class="{'todo-item-del': scope.row.status}">{{scope.row.title}}</div>
                        </template>
                    </el-table-column>
                    <el-table-column width="60">
                        <template>
                            <i class="el-icon-edit"></i>
                            <i class="el-icon-delete"></i>
                        </template>
                    </el-table-column>
                </el-table>
            </el-card>
        </el-col>
    </el-row>
</div>
</template>

<script>
export default {
    name: 'dashboard',
    data() {
        return {
            todoList: [{
                    title: '今天要修复1000行代码',
                    status: false,
                },
                {
                    title: '今天要写1000行代码加几个bug吧',
                    status: false,
                }, {
                    title: '今天要修复100个bug',
                    status: false,
                },
                {
                    title: '今天要修复100个bug',
                    status: true,
                },
                {
                    title: '今天要写100行代码加几个bug吧',
                    status: true,
                },
                {
                    title: '今天要写100行代码加几个bug吧',
                    status: true,
                }
            ]
        }
    }
}
</script>

<style scoped>
.el-row {
    margin-bottom: 20px;
}

.grid-content {
    display: flex;
    align-items: center;
    height: 100px;
}

.grid-cont-right {
    flex: 1;
    text-align: center;
    font-size: 14px;
    color: #999;
}

.grid-num {
    font-size: 30px;
    font-weight: bold;
}

.grid-con-icon {
    font-size: 50px;
    width: 100px;
    height: 100px;
    text-align: center;
    line-height: 100px;
    color: #fff;
}

.grid-con-1 .grid-con-icon {
    background: linear-gradient(270deg, #3ea7d8, #5ec8e2);
}

.grid-con-1 .grid-num {
    color: #3ea7d8;
}

.grid-con-2 .grid-con-icon {
    background: linear-gradient(270deg, #7286de, #8991ed);
}

.grid-con-2 .grid-num {
    color: #7286de;
}

.grid-con-3 .grid-con-icon {
    background: linear-gradient(270deg, #fc878d, #ff9981);
}

.grid-con-3 .grid-num {
    color: #fc878d;
}

.user-info {
    display: flex;
    align-items: center;
    padding-bottom: 20px;
    border-bottom: 2px solid #ccc;
    margin-bottom: 20px;
}

.user-avator {
    width: 120px;
    height: 120px;
    border-radius: 50%;
}

.user-info-cont {
    padding-left: 50px;
    flex: 1;
    font-size: 14px;
    color: #999;
}

.user-info-cont div:first-child {
    font-size: 30px;
    color: #222;
}

.user-info-list {
    font-size: 14px;
    color: #999;
    line-height: 25px;
}

.user-info-list span {
    margin-left: 70px;
}

.mgb40 {
    margin-bottom: 40px;
}

.todo-item {
    font-size: 14px;
}

.todo-item-del {
    text-decoration: line-through;
    color: #999;
}

.schart {
    width: 100%;
    height: 300px;
}
</style>
```

