import Vue from 'vue'
import VueRouter from 'vue-router'

import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    name: 'Login',
    path: '/login',
    component: () => import('@/views/login/login.vue') 
    /*
        路由 懒加载
        的优点： 在需要的时候 进行加载， 节省空间提高效率
    */ 
  },
  {
    name: 'main',
    path: '/', 
    component: () => import('@/layout'),
    redirect: '/dashbord',
    children: [
      {
        name: 'Dashbord',
        path: '/dashbord', 
        component: () => import('@/views/dashbord')
      }
      // 菜单 路由 使用 接口数据 动态配置路由
      // {
      //   name: 'Users',
      //   path: '/users', 
      //   component: () => import('@/views/users')
      // },
      // {
      //   name: 'Roles',
      //   path: '/roles', 
      //   component: () => import('@/views/rights/roles.vue')
      // },
      // {
      //   name: 'Rights',
      //   path: '/rights', 
      //   component: () => import('@/views/rights/rights.vue')
      // }
    ]
  },
  {
    name: '404',
    path: '*', // * 可以 匹配任何路径， 但是前提是 router 中没有相应的路径配置才可以， 
    component: () => import('@/views/404')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

/*
自定义 方法解决 动态路由重复加载问题
自定义一个 $addRoutes 方法 在这个方法中重新创建 新的 router 实例

 注意： 这种方式 只适用于 history 模式
*/ 
router.$addRoutes = (params) => {
  router.matcher = new VueRouter({
                        mode: 'history',
                        routes: []
                      }).matcher
  router.addRoutes(params)
}


// 导航守卫
router.beforeEach(function (to, from, next) {
  if (to.path === '/login') {
    next()
  } else {
    // 判断是否有 token 如果有 token 就放行 否则跳转到登录页面
    if (store.state.token) {
      next()
    }else {
      next('/login')
    }
  }
})

export default router
