import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// 配置菜单图标
import menusIcon from './menusIcon.json'

export default new Vuex.Store({
  state: {
    /*
      state 中设置 token 的默认值 是从 localStorage 中读取的
      Vuex 实例在创建的时候 就设置好了默认值
      后期 在设置 数据到 localStorage 是不会影响 state 中的 token 数据
    */ 
    token: localStorage.getItem('cms-token'),
    username: localStorage.getItem('username'),
    opened: true,
    /*
      根据所有 菜单的 路径（path属性）
      自己配置 菜单图标 一定是 根 路径一一对应关系， 就可以 在对用的菜单栏展示对应的图标
      */ 
     menusIcon: menusIcon
  },
  mutations: {
    // 设置 token
    // 你可以向 store.commit 传入额外的参数，即 mutation 的 载荷（payload）：
    setToken(state, payload) { // payload 载荷
      state.token = payload
       /*
        保存用户名和 token （持久化 用户名和token）
        Vuex 保存的数据 在 内存中，读取速度 比 localStorage 读取数据的速度快
        localStorage 保存在数据 在 磁盘上
      */
      localStorage.setItem("cms-token", payload);
    },
    removeToken(state) {
      state.token = null
      localStorage.removeItem('cms-token')
    },
    // 设置用户名
    setUsername(state, payload) {
      state.username = payload
      localStorage.setItem("username", payload);
    },
    removeUsername(state) {
      state.username = null
      localStorage.removeItem('username')
    },
    // 设置 侧边栏是否关闭或展开
    setSideBarOpen(state) {
      state.opened = !state.opened
    }
  },
  actions: {
  },
  modules: {
  }
})
