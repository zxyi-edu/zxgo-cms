import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 完整引入 Element-UI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

// 全局初始化样式设置引入
import '@/style/index.scss'

// 导入 折线图组件
import VeLine from 'v-charts/lib/line.common'
// 导入 柱状图组件
import VeHistogram from 'v-charts/lib/histogram.common'
// 导入 饼状图组件
import VePie from 'v-charts/lib/pie.common'

Vue.component(VeLine.name, VeLine)
Vue.component(VeHistogram.name, VeHistogram)
Vue.component(VePie.name, VePie)


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
