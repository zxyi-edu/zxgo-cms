// 获取报表数据
import $api from '@/api'

export const getReports = (data) => {
    return $api.get('reports/type/' + data)
}