import axios from 'axios'
import Qs from 'qs' //将请求参数 对象转换成 表单格式
// 单独 引入 element UI 组件
import { Loading, Message } from 'element-ui';

import store from '@/store'
import router from '@/router'

// 可以使用自定义配置新建一个 axios 实例]
const $axios = axios.create({
    // 基础 URL， 在请求时会拼接在 网络请求的地址前面
    baseURL: 'http://www.lovegf.cn:8888/api/private/v1/',
    /*
    开发项目： 一般情况分为三个阶段 
    第一个阶段： 开发阶段， 开发阶段会 开发阶段对应的 服务器地址
    第二个阶段： UAT阶段， 用户测试阶段 
    第三个阶段： 生产发布阶段， 
    */ 

    // 设置 超时时长
    timeout: 30000,
})

// 定义个 loading 变量
let loading = null

// 请求全局拦截器 对请求做统一处理
// 添加请求拦截器
$axios.interceptors.request.use(function (config) {
    // 开启 Loadgin窗口
    loading = Loading.service({text: '拼命加载中'});

    // 获取本地 token 
    // const token = localStorage.getItem('cms-token')
    if(store.state.token) {
        // 有 token 就设置 到 请求头中
        config.headers['Authorization'] = store.state.token
    }

    return config;
  }, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  });

// 添加响应拦截器
$axios.interceptors.response.use(function (response) {
    // 如果 有 loading 窗口 那么就关闭 loading
    if(loading) {
        loading.close()
    }

    // 有响应数据 判断 服务端 返回的状态
    const code = response.status
    if ((code >= 200 && code < 300) || code === 304) { // 304 重定向

      // 判断 token 数据是否过期 拦截响应数据
      if (response.data.meta.status == 400) {
        // 1.清空本地用户信息
        store.commit('removeToken')
        store.commit('removeUsername')
        // 2.跳转登陆页面
        router.replace('/login')
        // 3. 提示错误信息
        Message.error(response.data.meta.msg)
        return Promise.reject(response) // 返回一个失败信息
      }
      return Promise.resolve(response.data)
    } else {
      return Promise.reject(response)
    }
  }, function (error) {
    // 对响应错误做点什么
    if (loading) {
        loading.close()
      }
    if (error.response) {
    switch (error.response.status) {
        case 404:
        Message.error('网络请求不存在')
        break
        default:
        Message.error(error.response.data.message)
    }
    } else {
    // 请求超时或者网络有问题
    if (error.message.includes('timeout')) {
        Message.error('请求超时！请检查网络是否正常')
    } else {
        Message.error('请求失败，请检查网络是否已连接')
    }
    }
    return Promise.reject(error)
  });



function post(url, data) {
    return $axios({
      method: 'post',
      url,
      data: Qs.stringify(data),
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    })
}

function get(url, params) {
    return $axios({
      method:'get',
      url,
      params
    })
}

  export default {
      post,
      get
  }