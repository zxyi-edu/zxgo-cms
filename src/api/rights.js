import $api from '@/api'

export const getMenus = (data) => {
    return $api.get('menus', data)
}