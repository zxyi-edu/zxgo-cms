import $api from '@/api'

// 1.2.1. 用户数据列表
export const users = (data) => {
    return $api.post('users', data)
}

// 1.2.2. 添加用户
// 1.2.3. 修改用户状态
// 1.2.4. 根据ID查询用户信息
// 1.2.5. 编辑用户提交
// 1.2.6. 删除单个用户
// 1.2.7. 分配用户角色



